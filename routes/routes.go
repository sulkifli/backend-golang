package routes

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"go-app/controllers"
	"go-app/middleware"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()

    corsConfig := cors.DefaultConfig()
    corsConfig.AllowAllOrigins = true
    corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

    // To be able to send tokens to the server.
    corsConfig.AllowCredentials = true
    // OPTIONS method for ReactJS
    corsConfig.AddAllowMethods("OPTIONS")

    r.Use(cors.New(corsConfig))

    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })

    // auth
    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)

    // user
    UserMiddlewareRoute := r.Group("/user")
    UserMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		UserMiddlewareRoute.GET("/detail", controllers.GetUserLogin)
	}


    // category
    r.GET("/categories", controllers.GetAllCategory)
    r.GET("/categories/:id", controllers.GetCategoryById)
    moviesMiddlewareRoute := r.Group("/categories")
    moviesMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		moviesMiddlewareRoute.POST("", controllers.CreateCategory)
		moviesMiddlewareRoute.PUT("/:id", controllers.UpdateCategory)
		moviesMiddlewareRoute.DELETE("/:id", controllers.DeleteCategory)
	}

    // items
    r.GET("/items", controllers.GetAllItem)
    r.GET("/items/:id", controllers.GetItemById)
    itemMiddlewareRoute := r.Group("/items")
    itemMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		itemMiddlewareRoute.POST("", controllers.CreateItem)
		itemMiddlewareRoute.PUT("/:id", controllers.UpdateItem)
		itemMiddlewareRoute.DELETE("/:id", controllers.DeleteItem)
	}

    // feature
    r.GET("/features", controllers.GetAllFeature)
    r.GET("/features/:id", controllers.GetFeatureById)
    featureMiddlewareRoute := r.Group("/features")
    featureMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		featureMiddlewareRoute.POST("", controllers.CreateFeature)
		featureMiddlewareRoute.PUT("/:id", controllers.UpdateFeature)
		featureMiddlewareRoute.DELETE("/:id", controllers.DeleteFeature)
	}

    // image
    r.GET("/images", controllers.GetAllImage)
    r.GET("/images/:id", controllers.GetImageById)
    imageMiddlewareRoute := r.Group("/images")
    imageMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		imageMiddlewareRoute.POST("", controllers.CreateImage)
		imageMiddlewareRoute.PUT("/:id", controllers.UpdateImage)
		imageMiddlewareRoute.DELETE("/:id", controllers.DeleteImage)
	}

    // booking
    bookingMiddlewareRoute := r.Group("/bookings")
    bookingMiddlewareRoute.Use(middleware.JwtAuthMiddleware())
	{
		bookingMiddlewareRoute.GET("", controllers.GetAllBooking)
		bookingMiddlewareRoute.GET("/:id", controllers.GetBookingById)
		bookingMiddlewareRoute.POST("", controllers.CreateBooking)
	    bookingMiddlewareRoute.PUT("/:id", controllers.UpdateBooking)
		bookingMiddlewareRoute.DELETE("/:id", controllers.DeleteBooking)
	}

    // swagger
    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r

}