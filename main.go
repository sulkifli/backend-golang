package main

import (
	"go-app/config"
	"go-app/docs"
	"go-app/routes"
	"go-app/utils"
	"log"

	"github.com/joho/godotenv"
)

func main() {

	// for load godotenv
    // for env
    environment := utils.Getenv("ENVIRONMENT", "development")

    if environment == "development" {
    	err := godotenv.Load()
    	if err != nil {
        	log.Fatal("Error loading .env file")
        }
    }
	

	// main

	//programmatically set swagger info
	docs.SwaggerInfo.Title = "Staycation Documentation API"
	docs.SwaggerInfo.Description = "This is Documention for Staycation REST API ."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.Host = utils.Getenv("SWAGGER_HOST", "localhost:8080")
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}
