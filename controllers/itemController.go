package controllers

import (
	"go-app/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ItemInput struct {
	Title       string ` json:"title" binding:"required"`
	Price       int    ` json:"price" binding:"required"`
	City        string ` json:"city"`
	Description string ` json:"description"`
}

// GetAllItem godoc
// @Summary Get all Item.
// @Description Get a list of Item.
// @Tags Item
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /items [get]
func GetAllItem(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var data []models.Item
	db.Preload("Category").Preload("Feature").Preload("Image").Find(&data)
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": data })
}


// Get ItemById godoc
// @Summary Get Item.
// @Description Get a Item by id.
// @Tags Item
// @Produce json
// @Param id path string true " Item id"
// @Success 200 {object} map[string]interface{}
// @Router /items/{id} [get]
func GetItemById(c *gin.Context) { // Get model if exist
	var item models.Item

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).Preload("Category").Preload("Feature").Preload("Image").First(&item).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": item, })
}


// CreateItem godoc
// @Summary Create New Item.
// @Description Creating a new Item.
// @Tags Item
// @Param Body body ItemInput true "the body to create a new Item"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /items [post]
func CreateItem(c *gin.Context) {
	// Validate input
	var input ItemInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Item
	data := models.Item{Title: input.Title, Price: input.Price, City: input.City, Description: input.Description}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&data)

	c.JSON(http.StatusOK, gin.H{"code": "200", "message": "success create data", "data": data})
}

// UpdateItem godoc
// @Summary Update Item.
// @Description Update Item by id.
// @Tags Item
// @Produce json
// @Param id path string true "Item id"
// @Param Body body ItemInput true "the body to update age rating Item"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /items/{id} [put]
func UpdateItem(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if exist
	var item models.Item
	if err := db.Where("id = ?", c.Param("id")).First(&item).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input ItemInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Item
	updatedInput.Title = input.Title
	updatedInput.Price = input.Price
	updatedInput.City = input.City
	updatedInput.Description = input.Description
	updatedInput.UpdatedAt = time.Now()

	db.Model(&item).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success update data", "data": item, })

}

// DeleteItem godoc
// @Summary Delete one Item.
// @Description Delete a Item by id.
// @Tags Item
// @Produce json
// @Param id path string true "Item id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /items/{id} [delete]
func DeleteItem(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var item models.Item
	if err := db.Where("id = ?", c.Param("id")).First(&item).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	db.Delete(&item)

	c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success delete data"})
}
