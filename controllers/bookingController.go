package controllers

import (
	"go-app/models"
	"go-app/utils"
	"go-app/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type BookingInput struct {
	StartDate string ` json:"start_date" binding:"required" form:"start_date"`
	EndDate string ` json:"end_date" binding:"required" form:"end_date"`
	// ProofPayment string ` json:"proof_payment" binding:"required"`
	TotalPrice int ` json:"total_price" form:"total_price"`
	Night int ` json:"night" form:"night" `
	ItemID uint ` json:"item_id" binding:"required" form:"item_id"`
}

// GetAllBooking godoc
// @Summary Get all Booking.
// @Description Get a list of Booking.
// @Tags Booking
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /bookings [get]
func GetAllBooking(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var data []models.Booking
	db.Preload("Item").Preload("User").Find(&data)
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": data })
}

// Get BookingById godoc
// @Summary Get Booking.
// @Description Get a Booking by id.
// @Tags Booking
// @Produce json
// @Param id path string true " Booking id"
// @Success 200 {object} map[string]interface{}
// @Router /bookings/{id} [get]
func GetBookingById(c *gin.Context) { // Get model if exist
	var booking models.Booking

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).Preload("Item").Preload("User").First(&booking).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": booking, })
}

// CreateBooking godoc
// @Summary Create New Booking.
// @Description Creating a new Booking.
// @Tags Booking
// @Param Body body BookingInput true "the body to create a new Booking"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /bookings [post]
func CreateBooking(c *gin.Context) {
	formFile, _, _ := c.Request.FormFile("proof_payment")
	// Validate input
	var input BookingInput
	if err := c.Bind(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	uploadUrl, _ := utils.ImageUploadHelper(formFile)
	user_id, err := token.ExtractTokenID(c)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	
	// Create Category
	data := models.Booking{StartDate: input.StartDate, EndDate: input.EndDate, ProofPayment: uploadUrl, TotalPrice: input.TotalPrice, Night: input.Night ,ItemID:input.ItemID, UserID:user_id }
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&data)
	result := map[string]interface{}{
        "start_date": data.StartDate,
        "end_date":   data.EndDate,
        "proof_payment":   data.ProofPayment,
        "total_price":  data.TotalPrice,
        "night":  data.Night,
        "item_id":  data.ItemID,
        "user_id":  data.UserID,
    }
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success create data", "data": result })
}

// UpdateBooking godoc
// @Summary Update Booking.
// @Description Update Booking by id.
// @Tags Booking
// @Produce json
// @Param id path string true "Booking id"
// @Param Body body BookingInput true "the body to update age rating Booking"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /bookings/{id} [put]
func UpdateBooking(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var booking models.Booking
	if err := db.Where("id = ?", c.Param("id")).First(&booking).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	user_id, _ := token.ExtractTokenID(c)

	// Validate input
	var input BookingInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Booking
	updatedInput.StartDate = input.StartDate
	updatedInput.EndDate = input.EndDate
	// updatedInput.ProofPayment = input.ProofPayment
	updatedInput.TotalPrice = input.TotalPrice
	updatedInput.Night = input.Night
	updatedInput.UserID = user_id
	updatedInput.ItemID = input.ItemID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&booking).Updates(updatedInput)
	res := map[string]interface{}{
        "start_date": booking.StartDate,
        "end_date":   booking.EndDate,
        "proof_payment":   booking.ProofPayment,
        "total_price":  booking.TotalPrice,
        "night":  booking.Night,
        "item_id":  booking.ItemID,
        "user_id":  booking.UserID,
    }
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success update data", "data": res })
	
}


// DeleteBooking godoc
// @Summary Delete one Booking.
// @Description Delete a Booking by id.
// @Tags Booking
// @Produce json
// @Param id path string true "Booking id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /bookings/{id} [delete]
func DeleteBooking(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var booking models.Booking
	if err := db.Where("id = ?", c.Param("id")).First(&booking).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&booking)

	c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success delete data"})

}

