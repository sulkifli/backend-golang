package controllers

import (
	"fmt"
	"go-app/models"
	"go-app/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LoginInput struct {
    Email string `json:"email" binding:"required"`
    Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
    Name string `json:"name" binding:"required"`
    Phone string `json:"phone" binding:"required"`
    Password string `json:"password" binding:"required"`
    Email    string `json:"email" binding:"required"`
    Role    string `json:"role" binding:"required"`
}

// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input LoginInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}
    u.Email = input.Email
    u.Password = input.Password

    token, newuser , err := models.LoginCheck(u.Email, u.Password, db )


    if err != nil {
        fmt.Println(err)
        c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
        return
    }

    user := map[string]string{
        "name": newuser.Name,
        "phone": newuser.Phone,
        "email": newuser.Email,
        "role": newuser.Role,
    }

    c.JSON(http.StatusOK, gin.H{"message": "login success", "data": user, "token": token})

}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access.
// @Tags Auth
// @Param Body body RegisterInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input RegisterInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Name = input.Name
    u.Phone = input.Phone
    u.Email = input.Email
    u.Password = input.Password
    u.Role = input.Role

    _, err := u.SaveUser(db)

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user := map[string]string{
        "name": input.Name,
        "phone": input.Phone,
        "email":    input.Email,
        "role":    input.Role,
    }

    c.JSON(http.StatusOK, gin.H{"message": "registration success", "user": user})

}

// Get User By Login godoc
// @Summary Get User.
// @Description Get Detail User In Login.
// @Tags Auth
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /user/detail [get]
func GetUserLogin(c *gin.Context){

	user_id, err := token.ExtractTokenID(c)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	
	db := c.MustGet("db").(*gorm.DB)

	data,err := models.GetUserByID(user_id, db)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": data })

}