package controllers

import (
	"go-app/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ImageInput struct {
	Url    string ` json:"url" binding:"required"`
	ItemID uint   ` json:"item_id" binding:"required"`
}

// GetAllImage godoc
// @Summary Get all Image.
// @Description Get a list of Image.
// @Tags Image
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /images [get]
func GetAllImage(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var data []models.Image
	db.Find(&data)
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": data, })

}

// Get ImageById godoc
// @Summary Get Image.
// @Description Get a Image by id.
// @Tags Image
// @Produce json
// @Param id path string true " Image id"
// @Success 200 {object} map[string]interface{}
// @Router /images/{id} [get]
func GetImageById(c *gin.Context) { // Get model if exist
	var image models.Image

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&image).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": image, })

}


// CreateImage godoc
// @Summary Create New Image.
// @Description Creating a new Image.
// @Tags Image
// @Param Body body ImageInput true "the body to create a new Image"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /images [post]
func CreateImage(c *gin.Context) {
	// Validate input
	var input ImageInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Image
	data := models.Image{Url: input.Url, ItemID: input.ItemID}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&data)

	c.JSON(http.StatusOK, gin.H{"code": "200", "message": "success create data", "data": data})
}

// UpdateImage godoc
// @Summary Update Image.
// @Description Update Image by id.
// @Tags Image
// @Produce json
// @Param id path string true "Image id"
// @Param Body body ImageInput true "the body to update age rating Image"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /images/{id} [put]
func UpdateImage(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var image models.Image
	if err := db.Where("id = ?", c.Param("id")).First(&image).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input ImageInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Image
	updatedInput.Url = input.Url
	updatedInput.ItemID = input.ItemID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&image).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success update data", "data": image, })
	
}

// DeleteImage godoc
// @Summary Delete one Image.
// @Description Delete a Image by id.
// @Tags Image
// @Produce json
// @Param id path string true "Image id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /images/{id} [delete]
func DeleteImage(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var image models.Image
	if err := db.Where("id = ?", c.Param("id")).First(&image).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	db.Delete(&image)
	c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success delete data"})
}
