package controllers

import (
	"go-app/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type FeatureInput struct {
	Name   string ` json:"name" binding:"required"`
	Qty    int ` json:"qty" binding:"required"`
	ItemID uint   ` json:"item_id" binding:"required"`
}

// GetAllFeature godoc
// @Summary Get all Feature.
// @Description Get a list of Feature.
// @Tags Feature
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /features [get]
func GetAllFeature(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var data []models.Feature
	db.Find(&data)
    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": data, })

}

// Get FeatureById godoc
// @Summary Get Feature.
// @Description Get a Feature by id.
// @Tags Feature
// @Produce json
// @Param id path string true " Feature id"
// @Success 200 {object} map[string]interface{}
// @Router /features/{id} [get]
func GetFeatureById(c *gin.Context) { // Get model if exist
	var feature models.Feature

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success get data", "data": feature, })

}


// CreateFeature godoc
// @Summary Create New Feature.
// @Description Creating a new Feature.
// @Tags Feature
// @Param Body body FeatureInput true "the body to create a new Feature"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /features [post]
func CreateFeature(c *gin.Context) {
	// Validate input
	var input FeatureInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Feature
	data := models.Feature{Name: input.Name, Qty:input.Qty , ItemID: input.ItemID}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&data)

	c.JSON(http.StatusOK, gin.H{"code": "200", "message": "success create data", "data": data})
}

// UpdateFeature godoc
// @Summary Update Feature.
// @Description Update Feature by id.
// @Tags Feature
// @Produce json
// @Param id path string true "Feature id"
// @Param Body body FeatureInput true "the body to update age rating Feature"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /features/{id} [put]
func UpdateFeature(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var feature models.Feature
	if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input FeatureInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Feature
	updatedInput.Name = input.Name
	updatedInput.Qty = input.Qty
	updatedInput.ItemID = input.ItemID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&feature).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success update data", "data": feature, })
	
}

// DeleteFeature godoc
// @Summary Delete one Feature.
// @Description Delete a Feature by id.
// @Tags Feature
// @Produce json
// @Param id path string true "Feature id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /features/{id} [delete]
func DeleteFeature(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var feature models.Feature
	if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	db.Delete(&feature)
	c.JSON(http.StatusOK, gin.H{"code" : "200", "message": "success delete data"})
}
