package models

import (
	"time"
)

type Booking struct {
	ID                  uint      `json:"id" gorm:"primary_key"`
	StartDate           string    `gorm:"not null" json:"start_date" `
    EndDate             string    `gorm:"not null" json:"end_date" `
    ProofPayment        string    `gorm:"type:TEXT; not null" json:"proof_payment" `
    TotalPrice          int       `json:"total_price" `
    Night               int       `json:"night" `
	ItemID 				uint      `json:"-" `
	UserID 				uint      `json:"-" `
    Item Item			`json:"item"`
    User User 			`json:"user"`
	CreatedAt           time.Time `json:"created_at"`
	UpdatedAt           time.Time `json:"updated_at"`
}
