package models

import "time"

type Item struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	Title       string    `gorm:"not null" json:"title"`
	Price       int       `gorm:"not null" json:"price"`
	City        string    `json:"city"`
	Description string    `json:"description"`
	Category []Category   `gorm:"Foreignkey:ItemID;association_foreignkey:ID;" json:"category"`
	Feature []Feature  	  `gorm:"Foreignkey:ItemID;association_foreignkey:ID;" json:"feature"`
	Image []Image  	  	  `gorm:"Foreignkey:ItemID;association_foreignkey:ID;" json:"image"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}


