package models

import (
	"errors"
	"go-app/utils/token"
	"html"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type (
	// User
	User struct {
		ID        uint      `json:"id" gorm:"primary_key"`
		Name  	  string    `gorm:"not null" json:"username"`
		Phone  	  string    `gorm:"not null;unique" json:"phone"`
		Email     string    `gorm:"not null;unique" json:"email"`
		Password  string    `gorm:"not null;" json:"password"`
		Role  	  string    `gorm:"not null;" json:"role"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	}
)



func GetUserByID(uid uint, db *gorm.DB ) (User,error) {
	var u User

	if err := db.First(&u,uid).Error; err != nil {
		return u,errors.New("User not found!")
	}

	u.PrepareGive()
	
	return u,nil

}

func (u *User) PrepareGive(){
	u.Password = ""
}


func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func LoginCheck(email string, password string, db *gorm.DB) (string, User , error) {

	var err error

	u := User{}

	// fmt.Println(u)
	err = db.Model(User{}).Where("email = ?", email).Take(&u).Error

	if err != nil {
		return "", u,  err
	}

	err = VerifyPassword(password, u.Password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", u , err
	}

	token, err := token.GenerateToken(u.ID)

	if err != nil {
		return "", u , err
	}

	return token, u , nil

}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
    //turn password into hash
    hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
    if errPassword != nil {
        return &User{}, errPassword
    }
    u.Password = string(hashedPassword)
    //remove spaces in username
    u.Email = html.EscapeString(strings.TrimSpace(u.Email))

    var err error = db.Create(&u).Error
    if err != nil {
        return &User{}, err
    }
    return u, nil
}
