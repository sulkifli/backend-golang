package models

import "time"

type Image struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	Url       string    `gorm:"not null" json:"url" `
	ItemID    uint      `gorm:"column:item_id" json:"item_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
