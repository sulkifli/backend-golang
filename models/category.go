package models

import (
	"time"
)

type Category struct {
	ID                  uint      `json:"id" gorm:"primary_key"`
	Name                string    `gorm:"not null" json:"name" `
	ItemID 				uint     `gorm:"column:item_id" json:"item_id"`
	CreatedAt           time.Time `json:"created_at"`
	UpdatedAt           time.Time `json:"updated_at"`
}

